use std::cmp::{min, max};
use std::str::FromStr;
use std::u64;

use super::utils::grid;

#[derive(Debug, Copy, Clone)]
enum Direction {
    Right,
    Up,
    Left,
    Down,
}

impl Direction {
    pub fn delta(&self) -> (i64, i64) {
        match self {
            Direction::Right => (1, 0),
            Direction::Up => (0, 1),
            Direction::Left => (-1, 0),
            Direction::Down => (0, -1),
        }
    }

    pub fn rot_ccw(&self) -> Direction {
        match self {
            Direction::Right => Direction::Up,
            Direction::Up => Direction::Left,
            Direction::Left => Direction::Down,
            Direction::Down => Direction::Right,
        }
    }
}

struct SpiralPosIter {
    last_direction: Direction,
    next_pos: (i64, i64),
}

impl Iterator for SpiralPosIter {
    type Item = (i64, i64);

    fn next(&mut self) -> Option<Self::Item> {
        let (x, y) = self.next_pos;
        let next_level = Self::level(x, y);

        let (dx, dy) = self.last_direction.delta();
        let upper_level = next_level == Self::level(x + dx, y + dy);
        let bottom_right_corner =
            x == next_level as i64 &&
            -y == next_level as i64;
        if !(upper_level || bottom_right_corner) {
            self.last_direction = self.last_direction.rot_ccw();
        }
        let (rot_dx, rot_dy) = self.last_direction.delta();
        self.next_pos = (x + rot_dx, y + rot_dy);
        Some((x, y))
    }
 }

impl SpiralPosIter {
    fn level(x: i64, y: i64) -> u64 {
        max(x.abs(), y.abs()) as u64
    }

    pub fn new() -> Self {
        Self::new_at_level(0)
    }

    pub fn new_at_level(l: u64) -> Self {
        let start_pos = if l == 0 {
            (0, 0)
        } else {
            (l as i64, -(l as i64) + 1)
        };
        Self{
            last_direction: Direction::Right,
            next_pos: start_pos
        }
    }
}

pub fn solve1(input: &str) -> String {
    let loc = u64::from_str(input.trim()).unwrap();
    let dist = distance(loc);
    dist.to_string()
}

fn find_level_bound(n: u64) -> usize {
    let mut sum_lower_bound = 0;
    let mut l = 0;
    loop {
        sum_lower_bound += u64::pow(2 * l + 1, 2);
        if sum_lower_bound > n {
            return l as usize;
        }
        l += 1;
    }
}

pub fn solve2(input: &str) -> String {
    let n = u64::from_str(input.trim()).unwrap();
    let max_level = find_level_bound(n) + 1;

    let shape = (2 * max_level + 1, 2 * max_level + 1);
    let offset = (-(max_level as isize), -(max_level as isize));
    let mut grid = grid::DiscreteGrid::<u64>::fill(shape, 0);
    grid.shift(offset);

    let neigh_deltas = [(-1, -1), (-1, 0), (-1, 1),
                        (0, -1), (0, 1),
                        (1, -1), (1, 0), (1, 1)];

    grid[(0, 0)] = 1;
    for (x, y) in SpiralPosIter::new_at_level(1) {
        let mut value = 0;
        for (dx, dy) in &neigh_deltas {
            value += grid[((x+dx) as isize, (y+dy) as isize)]
        }

        if value > n {
            return value.to_string();
        } else {
            grid.insert((x as isize, y as isize), value);
        }
    }
    panic!("Never happens");
}

fn distance(loc: u64) -> u64 {
    let (x, y) = find_pos(loc);
    (x.abs() + y.abs()) as u64
}

fn find_ring(n: u64) -> Ring {
    Ring::iter().find(|r| r.min() <= n && r.max() >= n).unwrap()
}

fn find_pos(n: u64) -> (i64, i64) {
    let ring = find_ring(n);
    let (_a, b, c, d) = ring.sides();
    let (mut x, mut y) = ring.max_pos();
    let mut m = ring.max();

    let d_up = min(m - n, d - 1);
    y += d_up as i64;

    if n <= m - d {
        m -= d - 1;
        let c_right = min(m - n, c - 1);
        x += c_right as i64;

        if n <= m - c {
            m -= c - 1;
            let b_down = min(m - n, b - 1);
            y -= b_down as i64;

            if n <= m - b {
                m -= b - 1;
                let a_left = m - n;
                x -= a_left as i64;
            }
        }
    }

    (x, y)
}


#[derive(Debug, Clone)]
struct Ring {
    index: u64,
    min: u64,
}

impl Ring {
    fn index(&self) -> u64 {
        self.index
    }

    fn min(&self) -> u64 {
        self.min
    }

    fn min_pos(&self) -> (i64, i64) {
        if self.index == 0 {
            (0, 0)
        } else {
            let (x, y) = self.max_pos();
            (x + 2, y + 1)
        }
    }

    fn max(&self) -> u64 {
        self.min + self.size() - 1
    }

    fn max_pos(&self) -> (i64, i64) {
        let idx = self.index as i64;
        (-idx, -idx)
    }

    fn sides(&self) -> (u64, u64, u64, u64) {
        if self.index == 0 {
            (1, 1, 1, 1)
        } else {
            let d = 2 * self.index - 1;
            (d, d+1, d+2, d+2)
        }
    }

    fn size(&self) -> u64 {
        let (a, b, c, d) = self.sides();
        a + b + c + d - 3
    }

    fn succ(&self) -> Ring {
        Ring {
            index: self.index + 1,
            min: self.max() + 1,
        }
    }

    fn new(index: u64) -> Ring {
        let mut r = Ring {index: 0, min: 1};
        while r.index < index {
            r = r.succ();
        }
        r
    }

    fn iter() -> RingIter {
        RingIter {ring: Ring::new(0)}
    }
}

struct RingIter {
    ring: Ring,
}

impl Iterator for RingIter {
    type Item = Ring;

    fn next(&mut self) ->Option<Ring> {
        let r = self.ring.clone();
        self.ring = self.ring.succ();
        Some(r)
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_spiral_iter() {
        let mut i = SpiralPosIter::new();
        assert_eq!(i.next().unwrap(), (0, 0));
        assert_eq!(i.next().unwrap(), (1, 0));
        assert_eq!(i.next().unwrap(), (1, 1));
        assert_eq!(i.next().unwrap(), (0, 1));
        assert_eq!(i.next().unwrap(), (-1, 1));
        assert_eq!(i.next().unwrap(), (-1, 0));
        assert_eq!(i.next().unwrap(), (-1, -1));
        assert_eq!(i.next().unwrap(), (0, -1));
        assert_eq!(i.next().unwrap(), (1, -1));
        assert_eq!(i.next().unwrap(), (2, -1));
        assert_eq!(i.next().unwrap(), (2, 0));
    }

    #[test]
    fn test_spiral_bottom_right_corner_values() {
        let corners = SpiralPosIter::new()
            .zip(1..)
            .filter(|((x, y), _i)| *x > 0 && -y == *x);
        for ((x, _y), i) in corners.take(100) {
            assert_eq!(i, (2*x+1).pow(2));
        }
    }

    #[test]
    fn test_spiral_start_level() {
        let mut i = SpiralPosIter::new_at_level(2);
        assert_eq!(i.next().unwrap(), (2, -1));
        assert_eq!(i.next().unwrap(), (2, 0));
        assert_eq!(i.next().unwrap(), (2, 1));
    }

    #[test]
    fn test_find_level_bound() {
        assert_eq!(find_level_bound(1), 1);
        assert_eq!(find_level_bound(2), 1);
        assert_eq!(find_level_bound(9), 1);
        assert_eq!(find_level_bound(11), 2);
    }

    #[test]
    fn test_ring_size() {
        assert_eq!(Ring::new(0).size(), 1);
        assert_eq!(Ring::new(1).size(), 6);
        assert_eq!(Ring::new(2).size(), 14);
        assert_eq!(Ring::new(3).size(), 22);
    }

    #[test]
    fn test_ring_bounds() {
        assert_eq!(Ring::new(0).min(), 1);
        assert_eq!(Ring::new(0).max(), 1);

        assert_eq!(Ring::new(1).min(), 2);
        assert_eq!(Ring::new(1).max(), 7);

        assert_eq!(Ring::new(2).min(), 8);
        assert_eq!(Ring::new(2).max(), 21);

        assert_eq!(Ring::new(3).min(), 22);
        assert_eq!(Ring::new(3).max(), 43);
    }

    #[test]
    fn test_ring_iter() {
        let mut iter = Ring::iter();
        assert_eq!(iter.next().unwrap().index(), 0);
        assert_eq!(iter.next().unwrap().index(), 1);
        assert_eq!(iter.next().unwrap().index(), 2);
    }

    #[test]
    fn test_find_ring() {
        assert_eq!(find_ring(1).index(), 0);
        assert_eq!(find_ring(2).index(), 1);
        assert_eq!(find_ring(7).index(), 1);
        assert_eq!(find_ring(8).index(), 2);
        assert_eq!(find_ring(44).index(), 4);
    }

    #[test]
    fn test_ring_pos() {
        assert_eq!(Ring::new(0).min_pos(), (0,0));
        assert_eq!(Ring::new(0).max_pos(), (0,0));

        assert_eq!(Ring::new(1).min_pos(), (1,0));
        assert_eq!(Ring::new(1).max_pos(), (-1,-1));

        assert_eq!(Ring::new(3).min_pos(), (-1,-2));
        assert_eq!(Ring::new(3).max_pos(), (-3,-3));
    }

    #[test]
    fn test_find_pos() {
        assert_eq!(find_pos(1), (0, 0));
        assert_eq!(find_pos(2), (1, 0));
        assert_eq!(find_pos(4), (0, 1));
        assert_eq!(find_pos(15), (0, 2));
        assert_eq!(find_pos(12), (2, 1));
    }

    fn test_solve1(input: &str, expected: &str) {
        let output = solve1(input);
        assert_eq!(output.trim_end(), expected);
    }

    #[test]
    fn test_solve1_aoc1() {
        test_solve1("1", "0");
    }

    #[test]
    fn test_solve1_aoc2() {
        test_solve1("12", "3");
    }

    #[test]
    fn test_solve1_aoc3() {
        test_solve1("23", "2");
    }

    #[test]
    fn test_solve1_aoc4() {
        test_solve1("1024", "31");
    }

    fn test_solve2(input: &str, expected: &str) {
        let output = solve2(input);
        assert_eq!(output.trim_end(), expected);
    }

    #[test]
    fn test_solve2_aoc1() {
        test_solve2("1", "2");
    }

    #[test]
    fn test_solve2_aoc2() {
        test_solve2("5", "10");
    }

    #[test]
    fn test_solve2_aoc3() {
        test_solve2("746", "747");
    }

    #[test]
    fn test_solve2_aoc4() {
        test_solve2("747", "806");
    }
}
