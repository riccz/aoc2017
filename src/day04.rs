use std::collections::HashSet;

pub fn solve1(input: &str) -> String {
    let mut valid_cnt = 0;
    for line in input.lines() {
        if validate_passphrase(line.trim()) {
            valid_cnt += 1;
        }
    }
    valid_cnt.to_string()
}

fn validate_passphrase(pass: &str) -> bool {
    let mut words = HashSet::new();
    for w in pass.split_whitespace() {
        if !words.insert(w) {
            return false;
        }
    }
    true
}

pub fn solve2(input: &str) -> String {
    let mut valid_cnt = 0;
    for line in input.lines() {
        if validate_passphrase_anagrams(line.trim()) {
            valid_cnt += 1;
        }
    }
    valid_cnt.to_string()
}

fn validate_passphrase_anagrams(pass: &str) -> bool {
    let mut normalized_words = HashSet::new();
    for w in pass.split_whitespace() {
        let mut nw: Vec<char> = w.chars().collect();
        nw.sort();
        if !normalized_words.insert(nw) {
            return false;
        }
    }
    true
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_validate_passphrase() {
        assert_eq!(validate_passphrase("aa bb cc dd ee"), true);
        assert_eq!(validate_passphrase("aa bb cc dd aa"), false);
        assert_eq!(validate_passphrase("aa bb cc dd aaa"), true);
    }

    #[test]
    fn test_validate_passphrase_anagrams() {
        assert_eq!(validate_passphrase_anagrams("abcde fghij"), true);
        assert_eq!(validate_passphrase_anagrams("abcde xyz ecdab"), false);
        assert_eq!(validate_passphrase_anagrams("a ab abc abd abf abj"), true);
        assert_eq!(validate_passphrase_anagrams("iiii oiii ooii oooi oooo"), true);
        assert_eq!(validate_passphrase_anagrams("oiii ioii iioi iiio"), false);
    }
}
