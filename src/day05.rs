use std::i32;
use std::str::FromStr;

pub fn solve1(input: &str) -> String {
    let mut jumps: Vec<i32> = parse_lines(input).unwrap();

    let mut pos = 0;
    let mut count = 0;
    loop {
        count += 1;
        match do_jump(&mut jumps, pos) {
            Some(i) => pos = i,
            None => break
        }
    }

    count.to_string()
}

pub fn solve2(input: &str) -> String {
    let mut jumps: Vec<i32> = parse_lines(input).unwrap();

    let mut pos = 0;
    let mut count = 0;
    loop {
        count += 1;
        match do_jump_strange(&mut jumps, pos) {
            Some(i) => pos = i,
            None => break
        }
    }

    count.to_string()
}

fn parse_lines<T: FromStr>(input: &str) -> Result<Vec<T>, T::Err> {
    input.lines()
        .map(|l| l.trim())
        .map(|l| T::from_str(l))
        .collect()
}

fn do_jump(jump_table: &mut [i32], start: usize) -> Option<usize> {
    let dest = start as i32 + jump_table[start];
    jump_table[start] += 1;
    if dest >= 0 && (dest as usize) < jump_table.len() {
        Some(dest as usize)
    } else {
        None
    }
}

fn do_jump_strange(jump_table: &mut [i32], start: usize) -> Option<usize> {
    let offset = jump_table[start];
    let dest = start as i32 + offset;
    if offset >= 3 {
        jump_table[start] -= 1
    } else {
        jump_table[start] += 1
    }
    if dest >= 0 && (dest as usize) < jump_table.len() {
        Some(dest as usize)
    } else {
        None
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve1() {
        let input ="\
0
3
0
1
-3
";
        assert_eq!(solve1(input).trim_end(), "5");
    }

    #[test]
    fn test_solve2() {
        let input ="\
0
3
0
1
-3
";
        assert_eq!(solve2(input).trim_end(), "10");
    }
}
