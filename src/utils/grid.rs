use std::borrow::Borrow;
use std::cmp::{min, max};
use std::default::Default;
use std::fmt::Debug;
use std::isize;
use std::ops::{Index, IndexMut};


#[derive(Debug)]
pub struct DiscreteGrid<T>
where T: Default + Clone + Debug
{
    data: Vec<T>,
    shape: (usize, usize),
    offset: (isize, isize),
}

impl<T> DiscreteGrid<T>
where T: Default + Clone + Debug
{
    pub fn new() -> Self {
        Self {data: Vec::new(), shape: (0, 0), offset: (0, 0)}
    }

    pub fn fill<U>(shape: (usize, usize), value: U) -> Self
    where U: Borrow<T>
    {
        assert!(shape.0 <= isize::MAX as usize);
        assert!(shape.1 <= isize::MAX as usize);
        let data: Vec<T> = vec![value.borrow().clone(); shape.0 * shape.1];
        Self {data, shape, offset: (0, 0)}
    }

    pub fn shift(&mut self, delta: (isize, isize)) {
        self.offset.0 += delta.0;
        self.offset.1 += delta.1;
    }

    pub fn shape(&self) -> (usize, usize) {
        self.shape
    }

    pub fn offset(&self) -> (isize, isize) {
        self.offset
    }

    pub fn insert(&mut self, idx: (isize, isize), value: T) {
        if let LinIdx::OOB{..} = self.checked_linear_index(idx) {
            self.extend(idx);
        }
        *self.index_mut(idx) = value;
    }

    pub fn iter_rows<'a>(&'a self) -> DGridRowIter<'a, T> {
        DGridRowIter{grid: self, next_row: 0}
    }

    fn checked_linear_index(&self, idx: (isize, isize)) -> LinIdx {
        let i = idx.0 - self.offset.0;
        let j = idx.1 - self.offset.1;
        let lin_idx = i as usize + j as usize * self.shape.0;
        if !(i < self.shape.0 as isize && i >= 0) {
            LinIdx::OOB{lin_idx, axis: 0}
        } else if !(j < self.shape.1 as isize && j >= 0) {
            LinIdx::OOB{lin_idx, axis: 1}
        } else {
           LinIdx::Valid(lin_idx)
        }
    }

    fn linear_index(&self, idx: (isize, isize)) -> usize {
        match self.checked_linear_index(idx) {
            LinIdx::Valid(lin_idx) => lin_idx,
            LinIdx::OOB{lin_idx, axis} =>
                panic!("Linear index {} is out of the bounds of axis {}",
                       lin_idx, axis)
        }
    }

    fn extend(&mut self, idx: (isize, isize)) {
        let mut new_shape_x = max(self.shape.0 as isize,
                                  idx.0 - self.offset.0 + 1) as usize;
        let mut new_shape_y = max(self.shape.1 as isize,
                                  idx.1 - self.offset.1 + 1) as usize;

        new_shape_x += max(0, self.offset.0 - idx.0) as usize;
        new_shape_y += max(0, self.offset.1 - idx.1) as usize;

        assert!(new_shape_x <= isize::MAX as usize);
        assert!(new_shape_y <= isize::MAX as usize);

        let new_offset_x = min(self.offset.0, idx.0);
        let new_offset_y = min(self.offset.1, idx.1);

        let mut new_data = Vec::<T>::new();
        new_data.resize_with(new_shape_x * new_shape_y, Default::default);

        if self.shape.1 > 0 {
            let orig_rows = self.data.chunks(self.shape.1);
            let new_rows = new_data.chunks_mut(new_shape_y);
            let x_offset_diff = (self.offset.0 - new_offset_x) as usize;
            for (nr, or) in new_rows.skip(x_offset_diff).zip(orig_rows) {
                let y_offset_diff = (self.offset.1 - new_offset_y) as usize;
                let new_subrow = &mut nr[y_offset_diff
                                         ..
                                         y_offset_diff + self.shape.1];
                new_subrow.clone_from_slice(or);
            }
        }

        self.data = new_data;
        self.offset = (new_offset_x, new_offset_y);
        self.shape = (new_shape_x, new_shape_y);
    }
}

enum LinIdx {
    Valid(usize),
    OOB{lin_idx: usize, axis: usize}
}

impl<T> Index<(isize, isize)> for DiscreteGrid<T>
where T: Default + Clone + Debug
{
    type Output = T;

    fn index(&self, idx: (isize, isize)) -> &T {
        &self.data[self.linear_index(idx)]
    }
}

impl<T> IndexMut<(isize, isize)> for DiscreteGrid<T>
where T: Default + Clone + Debug
{
    fn index_mut<'a>(&'a mut self, idx: (isize, isize)) -> &'a mut T {
        let lin_idx = self.linear_index(idx);
        &mut self.data[lin_idx]
    }
}

pub struct DGridRowIter<'a, T>
where T: Default + Clone + Debug
{
    grid: &'a DiscreteGrid<T>,
    next_row: usize
}

impl<'a, T> Iterator for DGridRowIter<'a, T>
where T: Default + Clone + Debug
{
    type Item = &'a [T];

    fn next(&mut self) -> Option<Self::Item> {
        if self.next_row < self.grid.shape.0 {
            let start = self.next_row * self.grid.shape.0;
            let end = (self.next_row + 1) * self.grid.shape.0;
            self.next_row += 1;
            Some(&self.grid.data[start..end])
        } else {
            None
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_indexing() {
        let grid = DiscreteGrid::<char>::fill((3, 3), &'c');

        for x in 0..3 {
            for y in 0..3 {
                assert_eq!(grid[(x, y)], 'c');
            }
        }
    }

    #[test]
    fn test_iter_rows() {
        let mut grid = DiscreteGrid::<i32>::fill((3,3), &0);
        for x in 0..3 {
            for y in 0..3 {
                grid[(x, y)] = (x * y) as i32;
            }
        }

        let mut i = grid.iter_rows();
        assert_eq!(i.next().unwrap(), [0, 0, 0]);
        assert_eq!(i.next().unwrap(), [0, 1, 2]);
        assert_eq!(i.next().unwrap(), [0, 2, 4]);
        assert_eq!(i.next(), None);
    }

    #[test]
    #[should_panic]
    fn test_oob_indexing_1() {
        let grid = DiscreteGrid::<char>::fill((3, 3), &'c');
        grid[(-1, 0)];
    }

    #[test]
    #[should_panic]
    fn test_oob_indexing_2() {
        let grid = DiscreteGrid::<char>::fill((3, 3), &'c');
        grid[(0, -1)];
    }

    #[test]
    #[should_panic]
    fn test_oob_indexing_3() {
        let grid = DiscreteGrid::<char>::fill((3, 3), &'c');
        grid[(3, 1)];
    }

    #[test]
    #[should_panic]
    fn test_oob_indexing_4() {
        let grid = DiscreteGrid::<char>::fill((3, 3), &'c');
        grid[(1, 3)];
    }

    #[test]
    fn test_offset_indexing() {
        let mut grid = DiscreteGrid::<char>::fill((3, 3), &'c');

        grid.shift((1, 0));
        for x in 1..4 {
            for y in 0..3 {
                assert_eq!(grid[(x, y)], 'c');
            }
        }

        grid.shift((1, 3));
        for x in 2..5 {
            for y in 3..6 {
                assert_eq!(grid[(x, y)], 'c');
            }
        }
    }

    #[test]
    fn test_neg_offset_indexing() {
        let mut grid = DiscreteGrid::<char>::fill((3, 3), &'c');

        grid.shift((-1, 0));
        for x in -1..2 {
            for y in 0..3 {
                assert_eq!(grid[(x, y)], 'c');
            }
        }
    }

    #[test]
    #[should_panic]
    fn test_oob_offset_indexing_1() {
        let mut grid = DiscreteGrid::<char>::fill((3, 3), &'c');
        grid.shift((1, 0));
        grid[(0, 0)];
    }

    #[test]
    #[should_panic]
    fn test_oob_offset_indexing_2() {
        let mut grid = DiscreteGrid::<char>::fill((3, 3), &'c');
        grid.shift((-1, 0));
        grid[(2, 0)];
    }

    #[test]
    fn test_insert_from_empty() {
        let mut grid = DiscreteGrid::<char>::new();
        grid.insert((0,0), 'x');
        assert_eq!(grid[(0,0)], 'x');
    }

    #[test]
    fn test_idempotent_extend() {
        let mut grid = DiscreteGrid::<char>::new();
        grid.extend((9,9));
        assert_eq!(grid.shape(), (10, 10));
        grid.extend((9,9));
        assert_eq!(grid.shape(), (10, 10));
    }

    #[test]
    fn test_extend_with_offset() {
        let mut grid = DiscreteGrid::<char>::new();
        grid.extend((0,0));
        grid.extend((9, 4));
        grid.extend((9,-4));
        assert_eq!(grid.shape(), (10, 9));
        assert_eq!(grid.offset(), (0, -4));
    }

    #[test]
    fn test_insert_single_row() {
        let mut grid = DiscreteGrid::<char>::new();
        grid.insert((0,0), 'z');
        grid.insert((1,0), 'a');
        grid.insert((2,0), 'b');
    }
}
