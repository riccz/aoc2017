use std::u64;
use std::str::FromStr;

pub fn solve1(input: &str) -> String {
    let mut chksum = 0;
    for line in input.lines() {
        let mut max_line = u64::MIN;
        let mut min_line = u64::MAX;
        for raw_n in line.split_whitespace() {
            let n = u64::from_str(raw_n).unwrap();
            max_line = max_line.max(n);
            min_line = min_line.min(n);
        }
        chksum += max_line - min_line;
    }
    chksum.to_string()
}


fn find_divisible(ns: &[u64]) -> u64 {
    for (i, x) in ns.iter().enumerate() {
        for y in ns[i+1 .. ].iter() {
            if x % y == 0 {
                return x / y;
            } else if y % x == 0 {
                return y / x;
            }
        }
    }
    panic!("Never happens");
}


pub fn solve2(input: &str) -> String {
    let mut chksum = 0;
    for line in input.lines() {
        let mut ns: Vec<u64> = Vec::new();
        for raw_n in line.split_whitespace() {
            let n = u64::from_str(raw_n).unwrap();
            ns.push(n);
        }
        chksum += find_divisible(&ns);
    }
    chksum.to_string()
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve1_aoc1() {
        let input = "\
5 1 9 5
7 5 3
2 4 6 8";
        let expected = "18";
        let output = solve1(input);
        assert_eq!(output.trim_end(), expected);
    }

    #[test]
    fn test_solve2_aoc1() {
        let input = "\
5 9 2 8
9 4 7 3
3 8 6 5";
        let expected = "9";
        let output = solve2(input);
        assert_eq!(output.trim_end(), expected);
    }
}
