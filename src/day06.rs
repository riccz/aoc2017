use std::collections::{HashSet, HashMap};
use std::str::FromStr;
use std::u64;


pub fn solve1(input: &str) -> String {
    let mut cells = input.split_whitespace()
        .map(|n| u64::from_str(n))
        .collect::<Result<Vec<_>, _>>()
        .unwrap();

    let mut seen = HashSet::<Vec<u64>>::new();
    while seen.insert(cells.clone()) {
        balance(&mut cells);
    }
    let period = seen.len();

    period.to_string()
}

pub fn solve2(input: &str) -> String {
    let mut cells = input.split_whitespace()
        .map(|n| u64::from_str(n))
        .collect::<Result<Vec<_>, _>>()
        .unwrap();

    let mut seen = HashMap::<Vec<u64>, usize>::new();
    for i in 0.. {
        if let Some(old_i) = seen.insert(cells.clone(), i) {
            let period = i - old_i;
            return period.to_string();
        } else {
            balance(&mut cells);
        }
    }
    panic!("Never happens")
}

fn argmin<T: Ord + Copy>(cells: &[T]) -> Option<(usize, T)> {
    match cells.iter()
        .enumerate()
        .max_by_key(|(i, v)| (*v, -(*i as i64))) {
            None => None,
            Some((i, v)) => Some((i, *v)),
        }
}

fn balance(cells: &mut [u64]) {
    if let Some((min_i, min_blocks)) = argmin(cells) {
        cells[min_i] = 0;

        let same = min_blocks / cells.len() as u64;
        let rest = min_blocks % cells.len() as u64;

        for v in cells.iter_mut() {
            *v += same;
        }

        let assign_seq = (min_i+1..cells.len()).chain(0..min_i);
        for i in assign_seq.take(rest as usize) {
            cells[i] += 1;
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve1() {
        let input = "0 2 7 0";
        assert_eq!(solve1(input).trim_end(), "5");
    }

    #[test]
    fn test_solve2() {
        let input = "0 2 7 0";
        assert_eq!(solve2(input).trim_end(), "4");
    }

    #[test]
    fn test_balance_1() {
        let mut cells = vec![0, 2, 7, 0];
        balance(&mut cells);
        assert_eq!(cells, vec![2, 4, 1, 2]);
    }

    #[test]
    fn test_balance_2() {
        let mut cells = vec![2, 4, 1, 2];
        balance(&mut cells);
        assert_eq!(cells, vec![3, 1, 2, 3]);
    }

    #[test]
    fn test_balance_3() {
        let mut cells = vec![3, 1, 2, 3];
        balance(&mut cells);
        assert_eq!(cells, vec![0, 2, 3, 4]);
    }

    #[test]
    fn test_balance_4() {
        let mut cells = vec![0, 2, 3, 4];
        balance(&mut cells);
        assert_eq!(cells, vec![1, 3, 4, 1]);
    }

    #[test]
    fn test_balance_5() {
        let mut cells = vec![1, 3, 4, 1];
        balance(&mut cells);
        assert_eq!(cells, vec![2, 4, 1, 2]);
    }
}
