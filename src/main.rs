use std::fs;
use std::path::Path;
use std::time;

mod utils;

mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;


macro_rules! aoc {
    ($module:ident, $input1:expr) => {
        {
            println!("{}:", stringify!($module));
            let input1_fname = format!("data/{}", $input1);
            run_solve($module::solve1, input1_fname, "Answer 1");
        }
    };
    ($module:ident, $input1:expr, $input2: expr) => {
        aoc!($module, $input1);
        {
            let input2_fname = format!("data/{}", $input2);
            run_solve($module::solve2, input2_fname, "Answer 2");
        }
    };
}


fn run_solve<F, P>(func: F, input: P, label: &str)
where F: Fn(&str) -> String, P: AsRef<Path>
{
    let data = fs::read_to_string(input).unwrap();

    let start = time::Instant::now();
    let answer = func(&data);
    let elapsed = start.elapsed();

    println!("  {}: {}  ({} ms)", label, answer,
             elapsed.as_micros() as f64 / 1000. );
}


fn main() {
    aoc!(day01, "input_01_1.txt", "input_01_1.txt");
    aoc!(day02, "input_02_1.txt", "input_02_1.txt");
    aoc!(day03, "input_03_1.txt", "input_03_1.txt");
    aoc!(day04, "input_04_1.txt", "input_04_1.txt");
    aoc!(day05, "input_05_1.txt", "input_05_1.txt");
    aoc!(day06, "input_06_1.txt", "input_06_1.txt");
}
