fn read_digits(input: &str) -> Option<Vec<u64>> {
    let mut digits = Vec::new();
    for raw_d in input.chars() {
        if !raw_d.is_whitespace() {
            let d = raw_d.to_digit(10)?;
            digits.push(d as u64);
        }
    }
    Some(digits)
}

pub fn solve1(input: &str) -> String {
    let digits = read_digits(input).unwrap();
    let mut sum = 0;
    for xy in digits.windows(2) {
        match xy {
            &[x, y] => if x == y { sum += x; },
            _ => { panic!("Never happens"); }
        }
    }
    if digits.last().unwrap() == digits.first().unwrap() {
        sum += digits.last().unwrap();
    }
    sum.to_string()
}


pub fn solve2(input: &str) -> String {
    let digits = read_digits(input).unwrap();
    let mut shifted = digits.clone();
    assert_eq!(digits.len() % 2, 0);
    shifted.as_mut_slice().rotate_left(digits.len() / 2);
    let mut sum = 0;
    for (x, y) in digits.into_iter().zip(shifted.into_iter()) {
        if x == y {
            sum += x;
        }
    }
    sum.to_string()
}


#[cfg(test)]
mod tests {
    use super::*;
    use std::str;

    fn test_solve1(input: &str, expected: &str) {
        let output = solve1(input);
        assert_eq!(output.trim_end(), expected);
    }

    #[test]
    fn test_solve1_aoc1() {
        test_solve1("1122", "3");
    }

    #[test]
    fn test_solve1_aoc2() {
        test_solve1("1111", "4");
    }

    #[test]
    fn test_solve1_aoc3() {
        test_solve1("1234", "0");
    }

    #[test]
    fn test_solve1_aoc4() {
        test_solve1("91212129", "9");
    }

    fn test_solve2(input: &str, expected: &str) {
        let output = solve2(input);
        assert_eq!(output.trim_end(), expected);
    }

    #[test]
    fn test_solve2_aoc1() {
        test_solve2("1212", "6");
    }

    #[test]
    fn test_solve2_aoc2() {
        test_solve2("1221", "0");
    }

    #[test]
    fn test_solve2_aoc3() {
        test_solve2("123425", "4");
    }

    #[test]
    fn test_solve2_aoc4() {
        test_solve2("123123", "12");
    }

    #[test]
    fn test_solve2_aoc5() {
        test_solve2("12131415", "4");
    }
}
